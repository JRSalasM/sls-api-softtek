export enum TaskStatus {
  Created = "created",
  Pending = "pending",
  InProgress = "in_progress",
  Completed = "completed",
  Archived = "archived",
}

export class Task {
  readonly id: string;
  readonly title: string;
  readonly description: string;
  readonly status: TaskStatus;
  readonly createdAt: Date;
  readonly updatedAt?: Date;

  constructor(data: Task) {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.status = data.status;
    this.createdAt = data.createdAt;
    this.updatedAt = data.updatedAt;
  }
}
