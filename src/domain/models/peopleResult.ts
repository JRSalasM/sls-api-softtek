import { People } from "./people";

export class PeopleResult {
  nombre: string;
  altura: string;
  masa: string;
  color_del_cabello: string;
  color_de_piel: string;
  color_de_ojos: string;
  fecha_nacimiento: string;
  genero: string;
  planeta_natal: string;
  peliculas: string[];
  especies: string[];
  vehiculos: string[];
  naves_estelares: string[];
  creado: string;
  editado: string;
  url: string;

  constructor(data: People) {
    this.nombre = data.name;
    this.altura = data.height;
    this.masa = data.mass;
    this.color_del_cabello = data.hair_color;
    this.color_de_piel = data.skin_color;
    this.color_de_ojos = data.eye_color;
    this.fecha_nacimiento = data.birth_year;
    this.genero = data.gender;
    this.planeta_natal = data.homeworld;
    this.peliculas = data.films;
    this.especies = data.species;
    this.vehiculos = data.vehicles;
    this.naves_estelares = data.starships;
    this.creado = data.created;
    this.editado = data.edited;
    this.url = data.url;
  }
}
