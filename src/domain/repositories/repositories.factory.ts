import { TaskEntityMapper } from "../../infrastructure/mappers/task-entity.mapper";
import { DynamodbClientProvider } from "../../infrastructure/providers/dynamodb.provider";
import { DynamodbTaskRepository } from "../../infrastructure/repositories/dynamodb.task.repository";

export const createTaskRepository = (): DynamodbTaskRepository =>
  new DynamodbTaskRepository(
    new DynamodbClientProvider(),
    new TaskEntityMapper()
  );
