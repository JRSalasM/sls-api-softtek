import { Repository } from "./repository";
import { People } from "../models/people";

export interface PeopleRepository extends Repository<People> {
  findAll(): Promise<People[]>;
}
