import { Repository } from "./repository";
import { Task } from "../models/task";

export interface TaskRepository extends Repository<Task> {
  create(Task: Task): Promise<Task>;

  delete(id: string): Promise<void>;

  update(Task: Task): Promise<Task>;

  findById(TaskId: string): Promise<Task | null>;

  findAll(): Promise<Task[]>;
}
