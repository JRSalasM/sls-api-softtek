import {
  DeleteCommand,
  DeleteCommandInput,
  DynamoDBDocumentClient,
  GetCommand,
  GetCommandInput,
  PutCommand,
  PutCommandInput,
  ScanCommand,
  ScanCommandInput,
} from "@aws-sdk/lib-dynamodb";

import { Task } from "@domain/models/task";
import { TaskRepository } from "@domain/repositories/task.repository";

import { AWS_CONFIG } from "../config";
import { TaskEntity } from "../entities/task.entity";
import { TaskEntityMapper } from "../mappers/task-entity.mapper";
import { DynamodbClientProvider } from "../providers/dynamodb.provider";

export class DynamodbTaskRepository implements TaskRepository {
  private readonly tableName: string;
  private readonly docClient: DynamoDBDocumentClient;

  constructor(
    readonly dynamodbClientProvider: DynamodbClientProvider,
    readonly mapper: TaskEntityMapper
  ) {
    this.tableName = AWS_CONFIG.dynamodb.taskTable.name;
    this.docClient = dynamodbClientProvider.documentClient;
  }

  async create(task: Task): Promise<Task> {
    const taskEntity = this.mapper.toPersistenceEntity(task);

    console.log("taskEntity creating: ", taskEntity);

    const params: PutCommandInput = {
      TableName: this.tableName,
      Item: taskEntity,
    };
    await this.docClient.send(new PutCommand(params));
    return this.mapper.toDomainModel(taskEntity);
  }

  async findById(id: string): Promise<Task | null> {
    const params: GetCommandInput = {
      TableName: this.tableName,
      Key: { id },
    };

    const result = await this.docClient.send(new GetCommand(params));
    return result.Item ? this.mapper.toDomainModel(result.Item) : null;
  }

  async findAll(): Promise<Task[]> {
    const params: ScanCommandInput = {
      TableName: this.tableName,
    };

    const result = await this.docClient.send(new ScanCommand(params));
    return result.Items
      ? result.Items.map((item) => this.mapper.toDomainModel(item))
      : [];
  }

  async update(task: Partial<Task>): Promise<Task> {
    return this.mapper.toDomainModel({} as TaskEntity);
  }

  async delete(id: string): Promise<void> {
    const params: DeleteCommandInput = {
      TableName: this.tableName,
      Key: { id },
    };

    await this.docClient.send(new DeleteCommand(params));
  }
}
