import Axios from "axios";

import { People } from "@domain/models/people";
import { PeopleRepository } from "@domain/repositories/people.repository";

import { PeopleEntityMapper } from "../mappers/people-entity.mapper";

export class SwapiSeviceRepository implements PeopleRepository {
  private readonly urlBase: string;

  constructor(readonly mapper: PeopleEntityMapper) {
    this.urlBase = "https://swapi.py4e.com";
  }

  async findAll(): Promise<People[]> {
    const result = await Axios.get(`${this.urlBase}/api/people`);
    return result.data
      ? result.data.results.map((item) => this.mapper.toDomainModel(item))
      : [];
  }
}
