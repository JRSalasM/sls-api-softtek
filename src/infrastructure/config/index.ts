import * as process from "process";

import { DYNAMODB_CONFIG } from "./dynamodb.config";

export const AWS_CONFIG = {
  region: process.env.AWS_REGION,
  stage: process.env.STAGE,
  dynamodb: DYNAMODB_CONFIG,
};
