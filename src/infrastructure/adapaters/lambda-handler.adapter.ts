import {
  APIGatewayProxyEventV2,
  APIGatewayProxyEventV2WithJWTAuthorizer,
  APIGatewayProxyResultV2,
} from "aws-lambda";

import { Controller } from "@application/ports/controller";
import { IHttpRequest } from "@application/ports/http-request";

import { parseJsonRequestBody, toApiGwResponse } from "../helpers/api-gateway";

export const lambdaHandlerAdapter = <T>(
  controller: Controller<T>
): ((
  event: APIGatewayProxyEventV2 | APIGatewayProxyEventV2WithJWTAuthorizer
) => Promise<APIGatewayProxyResultV2>) => {
  return async (event) => {
    let { body } = event;

    if (event.headers["Content-Type"] === "application/json" && body) {
      body = parseJsonRequestBody(event.body);
    }

    const request: IHttpRequest = {
      body,
      params: event.pathParameters || {},
      query: event.queryStringParameters || {},
      headers: event.headers || {},
    };

    const response = await controller.handleRequest(request);

    return toApiGwResponse(response.statusCode, response.body);
  };
};
