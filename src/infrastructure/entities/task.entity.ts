import { TaskStatus } from "@domain/models/task";

export class TaskEntity {
  id: string;
  title: string;
  description: string;
  status: TaskStatus;
  createdAt: string;
  updatedAt?: string;

  constructor(taskEntity: TaskEntity) {
    this.id = taskEntity.id;
    this.title = taskEntity.title;
    this.description = taskEntity.description;
    this.status = taskEntity.status;
    this.createdAt = taskEntity.createdAt;
    this.updatedAt = taskEntity.updatedAt;
  }
}
