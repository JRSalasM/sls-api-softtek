export class PeopleEntity {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: string[];
  vehicles: string[];
  starships: string[];
  created: string;
  edited: string;
  url: string;

  constructor(peopleEntity: PeopleEntity) {
    this.name = peopleEntity.name;
    this.height = peopleEntity.height;
    this.mass = peopleEntity.mass;
    this.hair_color = peopleEntity.hair_color;
    this.skin_color = peopleEntity.skin_color;
    this.eye_color = peopleEntity.eye_color;
    this.birth_year = peopleEntity.birth_year;
    this.gender = peopleEntity.gender;
    this.homeworld = peopleEntity.homeworld;
    this.films = peopleEntity.films;
    this.species = peopleEntity.species;
    this.vehicles = peopleEntity.vehicles;
    this.starships = peopleEntity.starships;
    this.created = peopleEntity.created;
    this.edited = peopleEntity.edited;
    this.url = peopleEntity.url;
  }
}
