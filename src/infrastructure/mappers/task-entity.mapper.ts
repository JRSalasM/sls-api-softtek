import { Task } from "@domain/models/task";

import { IMapper } from "./mapper";
import { TaskEntity } from "../entities/task.entity";

export class TaskEntityMapper implements IMapper<Partial<TaskEntity>, Task> {
  public toDomainModel(taskEntity: Partial<TaskEntity>): Task {
    const { id, title, description, status, createdAt, updatedAt } = taskEntity;

    return new Task({
      id,
      title,
      description,
      status,
      createdAt: createdAt ? new Date(createdAt) : undefined,
      updatedAt: updatedAt ? new Date(updatedAt) : undefined,
    });
  }

  toPersistenceEntity(task: Partial<Task>): Partial<TaskEntity> {
    const { id, title, description, status, createdAt, updatedAt } = task;

    return new TaskEntity({
      id,
      title,
      description,
      status,
      createdAt: createdAt ? createdAt.toISOString() : undefined,
      updatedAt: updatedAt ? updatedAt.toISOString() : undefined,
    });
  }
}
