import { People } from "@domain/models/people";
import { PeopleResult } from "@domain/models/peopleResult";

import { IMapper } from "./mapper";
import { PeopleEntity } from "../entities/people.entity";

export class PeopleEntityMapper
  implements IMapper<Partial<PeopleEntity>, PeopleResult>
{
  public toDomainModel(peopleEntity: Partial<PeopleEntity>): PeopleResult {
    const {
      name,
      height,
      mass,
      hair_color,
      skin_color,
      eye_color,
      birth_year,
      gender,
      homeworld,
      films,
      species,
      vehicles,
      starships,
      created,
      edited,
      url,
    } = peopleEntity;

    return new PeopleResult({
      name,
      height,
      mass,
      hair_color,
      skin_color,
      eye_color,
      birth_year,
      gender,
      homeworld,
      films,
      species,
      vehicles,
      starships,
      created,
      edited,
      url,
    });
  }

  toPersistenceEntity(people: Partial<People>): Partial<PeopleEntity> {
    const {
      name,
      height,
      mass,
      hair_color,
      skin_color,
      eye_color,
      birth_year,
      gender,
      homeworld,
      films,
      species,
      vehicles,
      starships,
      created,
      edited,
      url,
    } = people;

    return new PeopleEntity({
      name,
      height,
      mass,
      hair_color,
      skin_color,
      eye_color,
      birth_year,
      gender,
      homeworld,
      films,
      species,
      vehicles,
      starships,
      created,
      edited,
      url,
    });
  }
}
