import { WithInterceptor } from "@application/decorators/interceptor.decorator";
import { Controller } from "@application/ports/controller";
import { IHttpRequest } from "@application/ports/http-request";
import { IHttpResponse } from "@application/ports/http-response";

import { GetPeopleUseCase } from "@application/use-cases/people/get-people.use-case";
import { People } from "@domain/models/people";

import { ErrorInterceptor } from "../../interceptors/error.interceptor";
import { SuccessHttpResponse } from "../../responses/http-response";

export class GetPeopleController implements Controller<People[]> {
  constructor(readonly listTodos: GetPeopleUseCase) {}

  @WithInterceptor(new ErrorInterceptor())
  async handleRequest(request: IHttpRequest): Promise<IHttpResponse<People[]>> {
    const todos = await this.listTodos.execute();
    return new SuccessHttpResponse(todos);
  }
}
