import { WithInterceptor } from "@application/decorators/interceptor.decorator";
import { Controller } from "@application/ports/controller";
import { IHttpRequest } from "@application/ports/http-request";
import { IHttpResponse } from "@application/ports/http-response";
import { GetTaskUseCase } from "@application/use-cases/task/get-task.use-case";
import { Task } from "@domain/models/task";

import { ErrorInterceptor } from "../../interceptors/error.interceptor";
import { SuccessHttpResponse } from "../../responses/http-response";

export class GetTaskController implements Controller<Task[]> {
  constructor(readonly listTodos: GetTaskUseCase) {}

  @WithInterceptor(new ErrorInterceptor())
  async handleRequest(request: IHttpRequest): Promise<IHttpResponse<Task[]>> {
    const todos = await this.listTodos.execute();
    return new SuccessHttpResponse(todos);
  }
}
