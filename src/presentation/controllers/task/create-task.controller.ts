import { WithInterceptor } from "@application/decorators/interceptor.decorator";
import { CreateTaskInput } from "@application/dtos/task/create-task.input";
import { Controller } from "@application/ports/controller";
import { IHttpRequest } from "@application/ports/http-request";
import { IHttpResponse } from "@application/ports/http-response";
import { CreateTaskUseCase } from "@application/use-cases/task/create-task.use-case";
import { Task } from "@domain/models/task";

import { ErrorInterceptor } from "../../interceptors/error.interceptor";
import { CreatedHttpResponse } from "../../responses/http-response";

export class CreateTaskController implements Controller<Task> {
  constructor(readonly createTask: CreateTaskUseCase) {}

  @WithInterceptor(new ErrorInterceptor())
  async handleRequest(
    request: IHttpRequest<CreateTaskInput>
  ): Promise<IHttpResponse<Task>> {
    const input: CreateTaskInput = { ...request.body };
    const task = await this.createTask.execute(input);
    return new CreatedHttpResponse(task);
  }
}
