import { IHttpRequest } from "@application/ports/http-request";
import { IHttpResponse } from "@application/ports/http-response";
import { Interceptor } from "@application/ports/interceptor";

export class LoggingInterceptor
  implements Interceptor<IHttpRequest, IHttpResponse>
{
  onRequest(request: IHttpRequest): IHttpRequest {
    console.log(request);
    return request;
  }

  onResponse<T>(response: IHttpResponse<T>): IHttpResponse<T> {
    console.log(response);
    return response;
  }
}
