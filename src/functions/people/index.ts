import { handlerPath } from "@libs/handler-resolver";
import { AWS } from "@serverless/typescript";
export type AWSFunctions = AWS["functions"];

export const peopleFunctions: AWSFunctions = {
  getAllPeople: {
    handler: `${handlerPath(__dirname)}/handler.getAllPeople`,
    events: [
      {
        http: {
          method: "get",
          path: "people",
        },
      },
    ],
  },
};
