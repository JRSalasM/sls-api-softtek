import { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from "aws-lambda";

import { lambdaHandlerAdapter } from "@infrastructure/adapaters/lambda-handler.adapter";
import { getPeopleController } from "@factories/controllers/people.controller.factory";

export const getAllPeople = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyResultV2> => {
  const handler = lambdaHandlerAdapter(getPeopleController());
  return await handler(event);
};
