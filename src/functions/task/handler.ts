import { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from "aws-lambda";

import { lambdaHandlerAdapter } from "@infrastructure/adapaters/lambda-handler.adapter";
import {
  createTaskController,
  getTaskController,
} from "@factories/controllers/task.controllers.factory";

export const createTask = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyResultV2> => {
  const handler = lambdaHandlerAdapter(createTaskController());
  return await handler(event);
};

export const getAllTask = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyResultV2> => {
  const handler = lambdaHandlerAdapter(getTaskController());
  return await handler(event);
};
