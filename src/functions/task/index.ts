import { handlerPath } from "@libs/handler-resolver";
import { AWS } from "@serverless/typescript";
export type AWSFunctions = AWS["functions"];

export const taskFunctions: AWSFunctions = {
  createTask: {
    handler: `${handlerPath(__dirname)}/handler.createTask`,
    events: [
      {
        http: {
          method: "post",
          path: "task",
        },
      },
    ],
  },
  getAllTask: {
    handler: `${handlerPath(__dirname)}/handler.getAllTask`,
    events: [
      {
        http: {
          method: "get",
          path: "task",
        },
      },
    ],
  },
};
