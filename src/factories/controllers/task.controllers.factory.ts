import { CreateTaskController } from "@presentation/controllers/task/create-task.controller";
import { GetTaskController } from "@presentation/controllers/task/get-task.controller";

import { CreateTaskUseCase } from "@application/use-cases/task/create-task.use-case";
import { GetTaskUseCase } from "@application/use-cases/task/get-task.use-case";

import { TaskValidator } from "@application/validators/task.validator";

import { createTaskRepository } from "@factories/repositories.factory";

export const createTaskController = (): CreateTaskController => {
  const createTaskUseCase = new CreateTaskUseCase(
    createTaskRepository(),
    new TaskValidator()
  );
  return new CreateTaskController(createTaskUseCase);
};

export const getTaskController = (): GetTaskController => {
  const getTaskUseCase = new GetTaskUseCase(createTaskRepository());
  return new GetTaskController(getTaskUseCase);
};
