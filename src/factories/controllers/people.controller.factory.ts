import { GetPeopleController } from "@presentation/controllers/people/get-people.controller";

import { GetPeopleUseCase } from "@application/use-cases/people/get-people.use-case";

import { swapiRepository } from "@factories/repositories.factory";

export const getPeopleController = (): GetPeopleController => {
  const getTaskUseCase = new GetPeopleUseCase(swapiRepository());
  return new GetPeopleController(getTaskUseCase);
};
