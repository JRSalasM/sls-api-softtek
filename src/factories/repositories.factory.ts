import { TaskEntityMapper } from "@infrastructure/mappers/task-entity.mapper";
import { DynamodbClientProvider } from "@infrastructure/providers/dynamodb.provider";
import { DynamodbTaskRepository } from "@infrastructure/repositories/dynamodb.task.repository";
import { PeopleEntityMapper } from "@infrastructure/mappers/people-entity.mapper";
import { SwapiSeviceRepository } from "@infrastructure/repositories/swapi.service.repository";

export const createTaskRepository = (): DynamodbTaskRepository =>
  new DynamodbTaskRepository(
    new DynamodbClientProvider(),
    new TaskEntityMapper()
  );

export const swapiRepository = (): SwapiSeviceRepository =>
  new SwapiSeviceRepository(new PeopleEntityMapper());
