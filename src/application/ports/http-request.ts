import { Dictionary, StringMap } from "@common/types";

export type HttpRequestAttributes = {} & Dictionary<string | any>;

export interface IHttpRequest<
  Body = unknown,
  Params = StringMap,
  Query = StringMap,
  Headers = StringMap,
  Attributes = HttpRequestAttributes
> {
  body?: Body;
  params?: Params;
  query?: Query;
  headers?: Headers;
  attributes?: Attributes;
}
