import { GetPeopleInput } from "@application/dtos/people/get-people.input";
import { People } from "@domain/models/people";
import { PeopleRepository } from "@domain/repositories/people.repository";

import { UseCase } from "../use-case";

export class GetPeopleUseCase implements UseCase<GetPeopleInput, People[]> {
  constructor(private readonly peopleRepository: PeopleRepository) {}

  async execute(): Promise<People[]> {
    return this.peopleRepository.findAll();
  }
}
