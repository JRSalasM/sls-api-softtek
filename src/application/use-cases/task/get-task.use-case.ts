import { GetTaskInput } from "@application/dtos/task/get-task.input";
import { Task } from "@domain/models/task";
import { TaskRepository } from "@domain/repositories/task.repository";

import { UseCase } from "../use-case";

export class GetTaskUseCase implements UseCase<GetTaskInput, Task[]> {
  constructor(private readonly taskRepository: TaskRepository) {}

  async execute(): Promise<Task[]> {
    return this.taskRepository.findAll();
  }
}
