import { v4 as uuidV4 } from "uuid";

import { Task, TaskStatus } from "@domain/models/task";
import { TaskRepository } from "@domain/repositories/task.repository";

import { CreateTaskInput } from "../../dtos/task/create-task.input";
import { BaseValidator } from "../../validators/abstract/base.validator";
import { UseCase } from "../use-case";

export class CreateTaskUseCase implements UseCase<CreateTaskInput, Task> {
  constructor(
    private readonly taskRepository: TaskRepository,
    private readonly validator: BaseValidator
  ) {}

  async execute(input: CreateTaskInput): Promise<Task> {
    this.validator.validateAndThrow(CreateTaskInput, input);

    const task = new Task({
      id: uuidV4(),
      title: input.title,
      description: input.description,
      status: input.status ?? TaskStatus.Created,
      createdAt: new Date(),
    });

    return await this.taskRepository.create(task);
  }
}
