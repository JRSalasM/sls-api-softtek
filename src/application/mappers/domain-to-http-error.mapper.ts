import {
  ApplicationError,
  AuthFailedError,
  BadRequestError,
  ConflictError,
  EntityNotFound,
  ForbiddenError,
  HttpError,
  InputValidationError,
  InternalServerError,
  NotFoundError,
  UnauthorizedError,
  UserAccountAlreadyExists,
  UserForbiddenError,
  UserNotAuthorizedError,
} from "../errors";

// Definiciones del interfaz para el objeto de mapeo de errores.
interface ErrorMapping {
  [key: string]: new (message: string) => HttpError;
}

// Cree un objeto de asignación de errores, que asigna los nombres de error del dominio a sus correspondientes constructores de errores HTTP.
const errorMappings: ErrorMapping = {
  [ApplicationError.name]: BadRequestError,
  [EntityNotFound.name]: NotFoundError,
  [InputValidationError.name]: BadRequestError,
  [AuthFailedError.name]: UnauthorizedError,
  [UserAccountAlreadyExists.name]: ConflictError,
  [UserForbiddenError.name]: ForbiddenError,
  [UserNotAuthorizedError.name]: UnauthorizedError,
};

// La función mapDomainErrorToHttpError toma un error de dominio y devuelve una instancia del error HTTP correspondiente.
export const mapDomainErrorToHttpError = (error: Error): HttpError => {
  const HttpErrorConstructor = errorMappings[error.constructor.name];

  if (HttpErrorConstructor) {
    return new HttpErrorConstructor(error.message);
  } else {
    // Si no se encuentra ningún error de dominio coincidente, devuelve un nuevo InternalServerError.
    return new InternalServerError(error.message);
  }
};
