import { Type } from "class-transformer";
import { ValidateNested } from "class-validator";

import { RequesterInfo } from "@application/dtos/requester-info.dto";

export class GetPeopleInput {
  requesterInfo: RequesterInfo;
}
