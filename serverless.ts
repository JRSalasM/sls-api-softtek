import type { AWS } from "@serverless/typescript";

import { taskFunctions } from "@functions/task";
import { peopleFunctions } from "@functions/people";

const serverlessConfiguration: AWS = {
  service: "sls-api-softtek",
  frameworkVersion: "3",
  plugins: [
    "serverless-esbuild",
    "serverless-offline",
    "serverless-dynamodb-local",
  ],
  provider: {
    name: "aws",
    runtime: "nodejs18.x",
    region: "us-east-1",
    // apiGateway: {
    //   minimumCompressionSize: 1024,
    //   shouldStartNameWithService: true,
    // },
    iamRoleStatements: [
      {
        Effect: "Allow",
        Action: ["dynamodb:*"],
        Resource: "arn:aws:dynamodb:us-east-1:905418342084:table/TaskTableDemo",
      },
    ],
    // environment: {
    //   AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
    //   NODE_OPTIONS: "--enable-source-maps --stack-trace-limit=1000",
    // },
  },
  // import the function via paths
  functions: { ...taskFunctions, ...peopleFunctions },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ["aws-sdk"],
      target: "node18",
      define: { "require.resolve": undefined },
      platform: "node",
      concurrency: 10,
    },
  },
  resources: {
    Resources: {
      TaskTable: {
        Type: "AWS::DynamoDB::Table",
        Properties: {
          TableName: "TaskTableDemo",
          BillingMode: "PAY_PER_REQUEST",
          AttributeDefinitions: [
            {
              AttributeName: "id",
              AttributeType: "S",
            },
          ],
          KeySchema: [
            {
              AttributeName: "id",
              KeyType: "HASH",
            },
          ],
        },
      },
    },
  },
};

module.exports = serverlessConfiguration;
