# Serverless - AWS Node.js Typescript

Este proyecto fue creado como reto Técnico - Backend NodeJS AWS

## Installation/deployment instructions

Configurar los credenciales de AWS
Ingresar el dato iamRoleStatements correspondiente a la tabla de dynamodb

### usando NPM

- Run `npm i` para instalar dependencias

### Locally

- Run `npm run offline`

### deploy

- Run `npm run deploy`
